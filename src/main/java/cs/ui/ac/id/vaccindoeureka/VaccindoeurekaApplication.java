package cs.ui.ac.id.vaccindoeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class VaccindoeurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaccindoeurekaApplication.class, args);
	}

}
